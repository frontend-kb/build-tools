See [State of JS 2022](https://2022.stateofjs.com/en-US/libraries/build-tools/)

[[_TOC_]]

Choice: **Vite**

## Ratios over time

![ratios over time](https://assets.devographics.com/captures/js2022/en-US/build_tools_experience_ranking.png "Ratios over time")

## Positive/negative split

![positive/negative split](https://assets.devographics.com/captures/js2022/en-US/build_tools_experience_marimekko.png "Positive/negative split")